
#' Prepare a DE experiment.
#'
#' Prepares the raw data for further analysis, calculates normalized expression
#' values and fits the data to the experiment design using limma.
#' 
#' @param counts matrix of raw read counts. Row names must contain feature identifiers 
#' and column names must contain sample names.
#' @param design data frame containing sample names in the first column and corresponding
#' group/condition information in the second column.
#' @param annotations optional data frame containing feature identifiers in the first column
#' and additional feature information in further columns.
#' @return An 'easyDEExperiment' object with following structure:
#' \describe{
#'   \item{fit:}{an object of limma::MArrayLM class}
#'   \item{dge:}{an object of edgeR::DGEList class}
#'   \item{expression:}{normalized expression values under variance stablilizng transformation}
#'   \item{counts:}{modified copy of the 'counts' argument}
#'   \item{design:}{copy of the 'design' argument}
#'   \item{annotations:}{modified copy of the 'annotations' argument}
#' }
#' @export
prepareExperiment = function(counts, design, annotations = NULL) {
    
    # Check if function arguments are correct
    # 'counts' should be a matrix of read counts
    assert_that(is.matrix(counts), is.integer(counts), all(counts >= 0))
    # 'design' should be a data.frame with 2 columns (sample, group/condition)
    assert_that(is.data.frame(design), ncol(design) == 2)
    # there must be at least 2 groups/conditions
    assert_that(length(levels(design[, 2])) >= 2)
    # there must be at least 3 biological replicates per group/condition
    assert_that(all(table(design[, 2]) >= 3))
    # column names of 'count' and sample names in 'design' must match exactly
    assert_that(identical(colnames(counts), as.vector(design[, 1])))
    if (!is.null(annotations)) {
        # 'annotations' should be a data.frame with at least 2 columns
        assert_that(is.data.frame(annotations), ncol(annotations) >= 2)
        # first column of 'annotations' should contain row names from 'count'
        assert_that(all(is.element(rownames(counts), annotations[, 1])))
    }

    # Prepare design matrix
    sample_group <- as.factor(design[, 2])
    design_matrix <- model.matrix(~ 0 + sample_group)
    colnames(design_matrix) <- gsub("^sample_group", "", colnames(design_matrix))

    # Remove features with low expression 
    counts <- counts[rowSums(edgeR::cpm(counts) > 1) >= 3, ]
    # Create DGEList object and normalize data
    dge <- edgeR::DGEList(counts = counts)
    dge <- edgeR::calcNormFactors(dge, method = "TMM")
    # Add annoations if available; rows are reordered so that they match rows of count table 
    if (!is.null(annotations)) {
        annotations <- annotations[match(rownames(counts), annotations[, 1]),]
        dge$genes <- annotations
    }    

    # Fit the data to the design using limma
    v <- limma::voom(dge, design_matrix)
    fit <- limma::lmFit(v, design_matrix)

    # Calculate normalized expression values under variance stablilizng transformation (DESeq 2)
    # Use these for heatmaps, PCA plots, machine learning etc.
    expression <- DESeq2::varianceStabilizingTransformation(counts)

    # Return an object of the easyDEExperiment class
    experiment <- list(fit = fit, dge = dge, expression = expression, counts = counts, design = design, annotations = annotations)
    class(experiment) <- "easyDEExperiment"
    return(experiment)
}


